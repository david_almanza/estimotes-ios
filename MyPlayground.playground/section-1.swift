// Playground - noun: a place where people can play

import Cocoa

let value = 3

switch value {
case 1:
    println("first value")
case 2:
    println("second value")
default:
    println("No one cares")
}