//
//  RoomsTest.swift
//  estimote
//
//  Created by DotCreek User on 2/9/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit
import XCTest

class RoomsTest: XCTestCase {
    let domain = "http://localhost:4000/"

    func testgetRoomsById() {
        // This is an example of a functional test case.
        let testRoomId = "8001CEE5-ADAE-4C3A-9DAA-59D61AD43214"
        let url = NSURL(string: (domain + "rooms?" + testRoomId))
        let data = NSURLConnection.sendSynchronousRequest((NSURLRequest(URL: url!)), returningResponse: nil, error: nil)
        let jsonObject:AnyObject = NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers, error: nil)!
        XCTAssertNotNil(jsonObject[0], "Value returned from servers must not be null")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
