//
//  BeaconTableViewCell.swift
//  estimote
//
//  Created by DotCreek User on 2/6/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit

protocol BeaconTableViewDelegate{
    func followBeacon(beacon: Beacons!)
}
class BeaconTableViewCell: UITableViewCell {

    var delegate:BeaconTableViewDelegate?
    
    var roomModel:Room = Room()
    var beaconModel:Beacons?
    
    @IBOutlet weak var lblRoomName: UILabel!
    
    @IBOutlet weak var lblRoomDetail: UILabel!
    
    @IBOutlet weak var btnFollowIBeacon: UIButton!
    
    func tintColorButton(){
        var imageStar = UIImage(named: "start-50")?.imageWithRenderingMode(.AlwaysTemplate)
        btnFollowIBeacon.setImage(imageStar, forState: .Normal)
    }
    
    @IBAction func addBeaconToFavorite(sender: AnyObject) {
        delegate?.followBeacon(beaconModel)
    }
    
    
    
}
