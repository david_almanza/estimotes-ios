//
//  BeaconTableViewControllers.swift
//  estimote
//
//  Created by DotCreek User on 2/6/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit
import CoreLocation

/*
*   Controller responsible to laod the table of beacons
*   Load all the beacons and the beacons to follow with their respective room asociation
*   Add beacons available to the beacons to follow
*/

class BeaconTableViewControllers: UITableViewController ,UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, BeaconTableViewDelegate {
    
    var beaconModel: Beacons?
    
    var beaconCell:BeaconTableViewCell?
    
    var beaconsRoom = [[Beacons: Room]]()
    
    var beaconsToFollow = [Beacons]()
    
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        
        //1-prepare localization for finding beacons
        locationManager = CLLocationManager()
        locationManager.delegate = self
        let regionBeacon = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"), identifier: "com.dotcreek.br1")
        locationManager.requestWhenInUseAuthorization()
        //self.locationManager.startRangingBeaconsInRegion(regionBeacon)
        
        //2-get beacons to list
        //-- List of beacons for testing
        let beacon1 = Beacons(idBeacon:"" ,uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D", majorValue: 1234, minorValue: 4321)
        let beacon2 = Beacons(idBeacon:"" ,uuid: "7B35D562-C3FA-4294-9948-3154610AE537", majorValue: 2345, minorValue: 5432)
        let beacon3 = Beacons(idBeacon:"" ,uuid: "BE4BCC26-5C3F-450A-BB66-8AA4B1804AF4", majorValue: 3456, minorValue: 6543)
        
        
        //3-get rooms per id
        
        //-- List of rooms to test
        let room1 = Room(id: "1234", name: "Room for DB", details: ["detailContent":"All about db", "detailTitle": "DBs"], image: "", dateCreated: NSDate())
        let room2 = Room(id: "89721", name: "PNUD conference", details: ["detailContent":"What is PNUD doing today", "detailTitle": "PNUD"], image: "", dateCreated: NSDate())
        let room3 = Room(id: "8203", name: "Shoe Sales Room", details: ["detailContent":"Winter shoes", "detailTitle": "expensive shoes"], image: "", dateCreated: NSDate())
        //create a list of objects {beacon:room}
        beaconsRoom = [ [beacon1: room1], [beacon2: room2], [beacon3: room3] ]
        
        Beacons.getBeaconsToFollow()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beaconsToFollowDidLoaded:", name: "com.dotcreek.beaconsToFollow.didLoad", object: nil)
        //get roomsId from events collection
        
        //get room with roomId
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beaconsRoom.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let beaconViewCell = tableView.dequeueReusableCellWithIdentifier("beaconCell") as BeaconTableViewCell
        beaconViewCell.delegate = self
        
        var roomModel:Room = Room()
        var beaconModel:Beacons
        for (beacon, room) in beaconsRoom[indexPath.row]{
            beaconModel = beacon
            roomModel = room
            beaconViewCell.roomModel = room
            beaconViewCell.beaconModel = beacon
        }
        beaconViewCell.lblRoomName.text = roomModel.name
        beaconViewCell.lblRoomDetail.text = roomModel.details["detailTitle"]
        
        return beaconViewCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        for (beacon, room) in beaconsRoom[indexPath.row]{
            beaconModel = beacon
        }
        self.performSegueWithIdentifier("beaconContentSegue", sender: nil);
    }
    
    @IBAction func veaconDetectMechanismChanged(sender: UISegmentedControl) {
        let selectedIndex = sender.selectedSegmentIndex
        
        switch selectedIndex{
        case 0:
            println("show All")
            
        case 1:
            println("show Followed")
            
        default:
            println("What did you choose?")
        }
    }
    
    //Mark: CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        println("number of estimotes \(beacons.count)")
    }
    
    //Mark: Notifications Selectors
    func beaconsToFollowDidLoaded(noti: NSNotification){
        let beacons = noti.object as [Beacons]
        println("Number of beacons \(beacons.count)")
        //for each beacon find room with respective id
        //create an array of dictianaries [[beacon: room]]
    }
    
    //Mark: BeaconTableViewDelegate
    func followBeacon(beacon: Beacons!) {
        println("Add beacon to favorite")
    }
    
    //Mark: Navigation Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier  == "beaconContentSegue"{
            let contentController = segue.destinationViewController as NotiBeaconContreoller
            contentController.beaconModel = beaconModel
            //self.navigationController?.pushViewController(contentController, animated: true)
        }
    }
    
}
