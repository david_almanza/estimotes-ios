//
//  NotiBeaconContreoller.swift
//  estimote
//
//  Created by DotCreek User on 2/3/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit

class NotiBeaconContreoller: UIViewController {
    //needed value to load the event information
    var beaconModel:Beacons?
    var eventModel:Event = Event()
    
    /*View Elements*/
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var wViewContent: UIWebView!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var imgViewContent: UIImageView!
    @IBOutlet weak var lblNoData: UILabel!
    
    override func viewDidLoad() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didNotFoundEventData", name: "com.dotcreek.errornoti.event.nodata", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "eventLoaded:", name: "com.dotcreek.datanoti.event.dataloaded", object: nil)
        
        if (beaconModel != nil){
            //load event information
            eventModel.getEventByBeaconId(beaconModel!.uuid)
            lblDescription.text = beaconModel?.descriptionLocation
        }else{
            lblNoData.text = "Sorry no beacon content found"
            showErrorMessage(true)
        }
    }
    
    func createWebViewHTMLText(content: String)->String{
        
        var htmlString:String = "<html><head><title></title></head><body style=\"background:transparent;\">"
        htmlString += content + "</body></html>"
        
        return htmlString
    }
    
    func showErrorMessage(flag: Bool){
        self.lblDescription.hidden = flag
        self.wViewContent.hidden = flag
        self.lblSummary.hidden = flag
        self.imgViewContent.hidden = flag
        self.lblNoData.hidden = !flag
    }
    
    // MARK: - Notification Center Selectors
    func eventLoaded(noti: NSNotification){
        self.eventModel = noti.object as Event
        self.wViewContent.loadHTMLString(createWebViewHTMLText(self.eventModel.contentModel.content), baseURL: nil)
        self.lblSummary.text = self.eventModel.summary
        showErrorMessage(false)
    }
    
    func didNotFoundEventData(){
        println("data not founded")
        showErrorMessage(true)
    }
    
    /* Dismiss NotiBeacon Controller */
    @IBAction func dismissViewController(sender: UIPanGestureRecognizer) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   
}
