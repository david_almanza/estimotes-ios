//
//  RoomsViewController.swift
//  estimote
//
//  Created by DotCreek User on 1/27/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit
import CoreLocation

/*
*   Controller where the location api start to receive all the beacons registered to be followed,
*   Where we register a local notification based on a CLBeaconRegion
*/

class RoomsViewController: UIViewController,CLLocationManagerDelegate {
    
    var locationManager:CLLocationManager!
    var actualCoordinate:CLLocationCoordinate2D?
    
    var beaconModel:Beacons?
    
    /*!
    * @brief Flag for indicating if a beacon was detected
    */
    var beaconNotificationDetected:Bool = false
    
    /*UI variables*/
    @IBOutlet weak var txtTest: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //attach notification listeners
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beaconFound:", name: "com.dotcreek.beacon.didLoad", object: nil)
        //obtain beacons to follow from server
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beaconsToFllow:", name: "com.dotcreek.beaconsToFollow.didLoad", object: nil)
        //Beacons.getBeaconsToFollow()
        
        // Do any additional setup after loading the view.
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        switch CLLocationManager.authorizationStatus(){
        case .NotDetermined:
            println("Access not determined")
            //asking access to localization software
            
        case .Restricted:
            println("Restricted hardware")
        case .Denied:
            println("Access Denied")
            let alertController = UIAlertController(title: "Can you change your settings", message: "We need access to your Location", preferredStyle: .Alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .Default, handler: {(action) in
                println("openning uiapplication")
                UIApplication.sharedApplication().openURL( NSURL(string: UIApplicationOpenSettingsURLString)!);
                return
            })
            alertController.addAction(settingsAction)
            let okAction = UIAlertAction(title: "Ok", style: .Default, handler:nil)
            alertController.addAction(okAction)
            presentViewController(alertController, animated: true, completion: nil)
        case .Authorized:
            println("Authorized access to hardware Always")
        case .AuthorizedWhenInUse:
            println("Authorized when in use")
            //locationManager.startUpdatingLocation();
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        if beaconNotificationDetected{
            //testing beacon
            beaconModel?.getBeaconsByIdByMajorByMinor()
            beaconNotificationDetected = false
        }else{
            println("Notification Not detected")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Start monitoring test ibeacon
    func startShowingNotifications(){
        println("Start showing noti")
        var locNoti = UILocalNotification()
        locNoti.alertBody = "You are near to an iBeacon"
        locNoti.regionTriggersOnce = false
        //let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"), major: 3138, minor: 9723, identifier: "com.dotcreek.beaconTest")
        let beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"), identifier: "")
        locNoti.region = beaconRegion
        /*start monitoring beacons too */
        //self.locationManager.startRangingBeaconsInRegion(beaconRegion)
        UIApplication.sharedApplication().scheduleLocalNotification(locNoti)

    }
    
    // MARK: CLLocationManagerDelegate
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error is \(error.description)")
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
        //println("number of beacons ", beacons.count)
        if beacons.count > 0{
            let nearestBeacon = beacons.last as CLBeacon
            switch nearestBeacon.proximity{
            case .Far:
                println("Beacon is far away")
            case .Near:
                println("Beacon is near you")
            case .Immediate:
                println("beacon with minor\(nearestBeacon.minor) and major\(nearestBeacon.major)")
                println("Beacon is rigth at you")
            case .Unknown:
                println("Beacon is at unkown position")
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            //register for notifications
            self.startShowingNotifications()
        }
    }
    
    // MARK: - Notification Center Selectors
    func beaconFound(noti: NSNotification){
        self.beaconModel = noti.object! as? Beacons
        //show to noti controller
        self.performSegueWithIdentifier("NotiBeaconSegue", sender: nil)
    }
    
    func beaconsToFllow(noti: NSNotification){
        let beaconsToFollow = noti.object as [Beacons]
        println("beacons To Follow \(beaconsToFollow.count)")
        for beacon in beaconsToFollow{
            println("Follow beacon: \(beacon.uuid) \(beacon.minor)")
            var localNoti = UILocalNotification()
            localNoti.region = beacon.beaconRegion
            localNoti.alertBody = "" //text that could be loaded from the server
            localNoti.regionTriggersOnce = true
            UIApplication.sharedApplication().scheduleLocalNotification(localNoti)
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "NotiBeaconSegue"){
            let controller:NotiBeaconContreoller = segue.destinationViewController as NotiBeaconContreoller
            controller.beaconModel = self.beaconModel
        }
    }
    
}
