//
//  AppDelegate.swift
//  estimote
//
//  Created by DotCreek User on 1/23/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        let notiUser = UIUserNotificationSettings(forTypes: .Alert, categories: nil)
        application.registerUserNotificationSettings(notiUser)
        
        //call RoomViewController
        /*let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let mainBarController:UITabBarController = mainSB.instantiateViewControllerWithIdentifier("mainTabController") as UITabBarController
        //2 is for room tab controller
        let viewControllerIndex = 2
        mainBarController.selectedIndex = viewControllerIndex
        let roomsViewController = mainBarController.viewControllers![2] as RoomsViewController
        roomsViewController.beaconNotificationDetected = true
        roomsViewController.beaconModel = Beacons(beaconRegion: CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"),major: 3138, minor: 9723, identifier: "com.dotcreek.beaconTest"))
        self.window?.rootViewController? = mainBarController*/
        
        return true
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        let region:CLBeaconRegion = notification.region as CLBeaconRegion
        var beacon:Beacons?
        println("reaching communication uuid:\(region.proximityUUID) major \(region.major) minor \(region.minor)")
        if (region != 0){
            beacon = Beacons(beaconRegion: region)
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

