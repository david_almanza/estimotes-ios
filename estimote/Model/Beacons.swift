//
//  Beacons.swift
//  estimote
//
//  Created by DotCreek User on 1/28/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import UIKit
import CoreLocation
//import Alamofire

class Beacons: NSObject, NSURLConnectionDataDelegate{
    
    var idBeacon:String?
    var uuid:String
    var createdAt:NSDate = NSDate()
    var updatedAt:NSDate = NSDate()
    var major:NSNumber = 0
    var minor:NSNumber = 0
    var descriptionLocation = ""
    var beaconRegion:CLBeaconRegion
    var urlEstimotes:URLStimotes
    
    //initializer for setting beacon id minor and major
    init(beaconRegion:CLBeaconRegion){
        self.urlEstimotes = URLStimotes()
        self.beaconRegion = beaconRegion
        self.major = beaconRegion.major
        self.minor = beaconRegion.minor
        self.uuid = beaconRegion.proximityUUID.UUIDString
        //println("Beacon uuid \(beaconRegion.proximityUUID)")
    }
    
    init(idBeacon: String, uuid: String, majorValue: NSNumber, minorValue: NSNumber) {
        self.idBeacon = idBeacon
        self.uuid = uuid
        self.major = majorValue
        self.minor = minorValue
        self.beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: uuid), identifier: "")
        //self.beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(UUIDString:uuid), major: NSNumber(unsignedShort: majorValue) , minor: NSNumber(unsignedShort: minorValue) , identifier: "com")

        self.urlEstimotes = URLStimotes()
    }
    
    /**
     * Get all the beacons to follow in the app and return a List of Beacons objects
    **/
    class func getBeaconsToFollow(){
        let urlSti = URLStimotes()
        var urlStringBeacons = urlSti.domainName + urlSti.pathBeacon
        let urlBeacons = NSURL(string: urlStringBeacons)
        
        NSURLConnection.sendAsynchronousRequest(NSURLRequest(URL: urlBeacons!), queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            var beaconsToFollow = [Beacons]()
            let jsonObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data,
                options: NSJSONReadingOptions.AllowFragments,
                error:nil)
            
            if let mainData = jsonObject as? NSArray{
                for beaconData in mainData{
                    if let beaconDataDict = beaconData as? NSDictionary{
                        let idBeacon = beaconDataDict["id"] as String
                        let uuidString = (beaconDataDict["uuid"] as String).uppercaseString
                        let majorNumberValue = beaconDataDict["major"] as NSNumber
                        let minorNumberValue = beaconDataDict["minor"] as NSNumber
                        let beaconModel = Beacons(idBeacon: idBeacon, uuid: uuidString, majorValue: majorNumberValue, minorValue:  minorNumberValue)
                        beaconsToFollow.append(beaconModel)
                    }
                }
            }//end if NSArray
            
            /*Beacons to Loaded follow Notification */
            NSNotificationCenter.defaultCenter().postNotificationName("com.dotcreek.beaconsToFollow.didLoad", object: beaconsToFollow)
        }
    }
    
    func getBeaconsByIdByMajorByMinor(){
        var urlStringBeacons:String = self.urlEstimotes.domainName + self.urlEstimotes.pathBeacon
        urlStringBeacons += "uuid=\(self.beaconRegion.proximityUUID.UUIDString)&major=\(self.beaconRegion.major)&minor=\(self.beaconRegion.minor)"
        let urlBeacons = NSURL(string: urlStringBeacons)
        let urlConne = NSURLConnection(request: NSURLRequest(URL: urlBeacons!), delegate: self, startImmediately: true)
    }
    
    //MARK: NSURLConnectionDataDelegate
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        //println("Response \(response.description)")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        let jsonObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil)
        
        if let dataBeacon = jsonObject as? NSDictionary{
            if let descriptionValue = dataBeacon["locationDescription"] as? String{
                self.descriptionLocation = descriptionValue
            }
            if let uuidValue = dataBeacon["uuid"] as? String{
                self.uuid = uuidValue
            }
        }
      
        //self.uuid = jsonObject
        //could load more data but need to change the service response
        /*
        self.createdAt = ...
        self.id = ...
        */
        NSNotificationCenter.defaultCenter().postNotificationName("com.dotcreek.beacon.didLoad", object: self)
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        println("Error is \(error.description)")
    }
}

