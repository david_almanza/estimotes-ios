//
//  Room.swift
//  estimote
//
//  Created by DotCreek User on 2/6/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import Foundation

class Room: NSObject{
    var id:String
    var name:String
    var details: [String: String]
    var image:String
    let dateCreated:NSDate
    
    override init(){
        self.id = ""
        self.name = ""
        self.details = [String: String]()
        self.image = ""
        self.dateCreated = NSDate()
    }
    
    init(id:String, name: String, details: [String: String], image:String,  dateCreated:NSDate){
        self.id = id
        self.name = name
        self.details = details
        self.image = image
        self.dateCreated = dateCreated
        super.init()
    }
    
    class func getRoomById(id: String){
        println("Data")
        let urlEstimotes = URLStimotes()
        let url = NSURL(string: (urlEstimotes.domainName + urlEstimotes.pathRooms + "roomId=" + id))
        let urlRequest = NSURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            let jsonObject: AnyObject = NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers, error: nil)!
            
            if let mainData = jsonObject as? NSArray{
                var idRoom = "", nameRoom = "", imageRoom = "", dateCreatedAtString = ""
                var dateCreated:NSDate = NSDate()
                var detailRoom :[String :String] = Dictionary(minimumCapacity: 2)
                //this service need to return a single array for now thats why oneData[0]
                if let roomData = mainData[1] as? NSDictionary{
                    //values from the surface
                    idRoom = roomData["id"] as String
                    nameRoom = roomData["name"] as String
                    imageRoom = roomData["image"] as String
                    dateCreatedAtString = roomData["createdAt"] as String
                    
                    if let detailsArray = roomData["details"] as? NSArray{
                        
                        if let detailData = detailsArray[0] as? NSDictionary{
                            if let detailContent = detailData["detailContent"] as? String{
                                println(detailContent)
                                detailRoom["detailContent"] = detailContent
                            }
                            if let detailTitle = detailData["detailTitle"] as? String{
                                println(detailTitle)
                                detailRoom["detailTitle"] = detailTitle
                            }
                        }
                    }
                }//end frstData
                
                let roomModel = Room(id: idRoom, name: nameRoom, details: detailRoom, image: imageRoom, dateCreated: self.convertStringDateToDate(dateCreatedAtString))
                NSNotificationCenter.defaultCenter().postNotificationName("com.dotcreek.roomLoaded", object: roomModel);
            }
        }
        
    }
    
    private class func convertStringDateToDate(string: String) -> NSDate{
        //ISODateFormate in mongo
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter.dateFromString(string)!
    }
    
}
