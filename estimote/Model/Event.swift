//
//  Events.swift
//  estimote
//
//  Created by DotCreek User on 2/4/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//
import Foundation

class Event: NSObject {
    var idEvent:String = ""
    var beaconModel:Beacons?
    var createdAt:NSDate = NSDate()
    var updatedAt:NSDate?
    var type:String = ""
    var summary:String = ""
    var startAt:NSDate = NSDate()
    var endAt:NSDate = NSDate()
    var id:String = ""
    var roomId:NSString?
    var contentId:NSString?
    var contentModel:Content
    
    
    let urlEstimotes:URLStimotes
    
    /*privates */
    private var dateFormatter:NSDateFormatter = NSDateFormatter()

    override init() {
        self.urlEstimotes = URLStimotes()
        contentModel = Content()
    }
    
    func getEventByBeaconId(beaconId: String){
        //upperCase beaconId
        let urlString = urlEstimotes.domainName + urlEstimotes.pathEvents + "beaconId=" + beaconId.uppercaseString
        let urlEvent:NSURL = NSURL(string: urlString)!
        NSURLConnection(request: NSURLRequest(URL:urlEvent), delegate: self, startImmediately: true)
        NSURLConnection.sendAsynchronousRequest(NSURLRequest(URL:urlEvent), queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            let jsonObject:AnyObject = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil)!
            if let arrayData = jsonObject as? NSArray{
                //arrayData comes into 2 arrays [[arrayData]]
                //for that reason needs to be flaten out arrayData[0]
                if arrayData.count >= 1 {
                    if let eventData = arrayData[0] as? NSDictionary{
                        if let createdAtEventData = eventData["createdAt"] as? String{
                            self.createdAt = self.convertStringDateToDate(createdAtEventData)
                        }
                        
                        if let updatedAtEventData = eventData["updatedAt"] as? String{
                            self.updatedAt = self.convertStringDateToDate(updatedAtEventData)
                        }
                        
                        if let startAtEventData = eventData["startAt"] as? String{
                            self.startAt = self.convertStringDateToDate(startAtEventData)
                        }
                        
                        if let endAtEventData = eventData["endAt"] as? String{
                            self.endAt = self.convertStringDateToDate(endAtEventData)
                        }
                        
                        if let summaryData = eventData["summary"] as? String{
                            self.summary = summaryData
                        }
                        
                        if let room = eventData["roomId"] as? String{
                            self.roomId = room
                        }
                        
                        if let content = eventData["contentId"] as? String{
                            self.contentId = content
                        }
                        
                        if let contentData:AnyObject = eventData["content"]?{
                            let content = contentData["content"] as String
                            let title = contentData["title"] as String
                            self.contentModel = Content(content: content, stringDate: contentData["createdAt"] as String, title: title)
                        }
                    }
                    NSNotificationCenter.defaultCenter().postNotificationName("com.dotcreek.datanoti.event.dataloaded", object: self)
                }//end if arrayData count
                else{
                    //No data to show
                    NSNotificationCenter.defaultCenter().postNotificationName("com.dotcreek.errornoti.event.nodata", object: nil)
                }
            }//end arrayData
            
        }
    }
    
    private func convertStringDateToDate(stringDate: String) -> NSDate{
        //ISODateFormate in mongo
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return dateFormatter.dateFromString(stringDate)!
        }
}
