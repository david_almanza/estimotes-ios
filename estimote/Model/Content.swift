//
//  Content.swift
//  estimote
//
//  Created by DotCreek User on 2/4/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//
import Foundation

class Content: NSObject {
    var content:String
    var title:String
    var stringDate:String?
    
    var updatedAt:NSDate? {
        get{
            if let string = stringDate{
                return convertStringDateToDate(stringDate!)
            }else{
                return nil
            }
        }
        set {
            
        }
    }
    
    /*privates */
    private var dateFormatter:NSDateFormatter = NSDateFormatter()
    
    override init() {
        self.content = ""
        self.title = ""
    }
    
    init(content:String, stringDate:String, title:String){
        self.content = content
        self.title = title
        self.stringDate = stringDate
    }
    
    private func convertStringDateToDate(stringDate: String) -> NSDate?{
        //ISODateFormate in mongo
        if (stringDate != NSNull()){
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return dateFormatter.dateFromString(stringDate)!
        }
        else {
            return nil
        }
    }

    
}
