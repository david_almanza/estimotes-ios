//
//  URLStimotes.swift
//  estimote
//
//  Created by DotCreek User on 1/29/15.
//  Copyright (c) 2015 DotCreek. All rights reserved.
//

import Foundation

struct URLStimotes {
    //change url for the domain or ip for the mongo server
    let domainName = "http://192.168.0.120:4000/"
    let pathBeacon = "beacons?"
    let pathContents = "contents?"
    let pathEvents = "events?"
    let pathNews = "news?"
    let pathRooms = "rooms?"
    let pathUsers = "users?"
}